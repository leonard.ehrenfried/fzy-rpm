Name:           fzy
Version:        1.0
Release:        1%{?dist}
Summary:        A better fuzzy finder

License:        MIT
URL:            https://github.com/jhawthorn/fzy
Source0:        https://github.com/jhawthorn/fzy/releases/download/1.0/fzy-1.0.tar.gz

BuildRequires:  gcc

%description
fzy is a fast, simple fuzzy text selector for the terminal with an advanced scoring algorithm.

%check
make check

%prep
%setup

%build
%make_build


%install
rm -rf $RPM_BUILD_ROOT
%make_install


%files
%license LICENSE
%doc ALGORITHM.md README.md
/usr/local/bin/%{name}
/usr/local/share/man/man1/%{name}.1

%changelog
* Tue Apr  9 2019 Leonard Ehrenfried <mail@leonard.io>
-
